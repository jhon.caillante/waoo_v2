import 'package:flutter/material.dart';
import 'package:waoo/src/pages/TabDireccion.dart';
import 'package:waoo/src/pages/TabImagenes.dart';
import 'package:waoo/src/pages/TabOpiniones.dart';
import 'package:waoo/src/pages/TabServicios.dart';
import 'package:waoo/src/pages/home_page.dart';
import 'package:waoo/src/pages/landig_page.dart';
import 'package:waoo/src/pages/negocios_detalle.dart';
import 'package:waoo/src/pages/negocios_page.dart';
import 'package:waoo/src/pages/nivel_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: 'ladingpage',
      routes:{
        'ladingpage':(BuildContext context)=> LandingPage(),
        'nivel':(BuildContext context)=> NivelPage(),
        'menu':(BuildContext context)=>HomePage(),
        'negocios':(BuildContext context)=>NegociosPage(),
        'detalle':(BuildContext context)=>Detalle(),
        'tabservicio':(BuildContext context)=>TabServicios(),
        'tabdireccion':(BuildContext context)=>TabDireccion(),
        'tabimagenes':(BuildContext context)=>TabImagenes(),
        'tabopiniones':(BuildContext context)=>TabOpiniones(),
      }
    );
  }
}