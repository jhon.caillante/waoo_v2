import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Contacto {
  final String tipo;
  final Icon icono;
  final String nombre;
  final String recurso;

  Contacto({
    @required this.tipo,
    @required this.icono,
    @required this.nombre,
    @required this.recurso
  });

  @override
  String toString() {
    return 'Contacto{tipo: $tipo, icono: $icono, nombre: $nombre, recurso: $recurso}';
  }


}
