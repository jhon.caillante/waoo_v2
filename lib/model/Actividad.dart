import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Actividad {
  final String id;
  final String nombre;
  final String detalle;
  final String imgPortada;
  final String direccion;
  final String whatsapp;
  final String celular;
  final String paginaWeb;
  final String paginaFacebook;
  final String correoElectronico;
  final String instagram;
  final String mapa;
  final String visitas;
  final String latitud;
  final String longitud;
  final String comentarios;
  final String puntuacion;
  final String delivery;
  final String horario;
  final String propietario;
  final String oferta;
  final String promocion;
  final String descuento;
  final String formasPago;


  Actividad({
    @required this.id,
    @required this.nombre,
    @required this.detalle,
    @required this.imgPortada,
    @required this.direccion,
    @required this.whatsapp,
    @required this.celular,
    @required this.paginaWeb,
    @required this.paginaFacebook,
    @required this.correoElectronico,
    @required this.instagram,
    @required this.mapa,
    @required this.visitas,
    @required this.latitud,
    @required this.longitud,
    @required this.comentarios,
    @required this.puntuacion,
    @required this.delivery,
    @required this.horario,
    @required this.propietario,
    @required this.oferta,
    @required this.promocion,
    @required this.descuento,
    @required this.formasPago

  });

  factory Actividad.fromJson(Map<dynamic, dynamic> json) {
    return Actividad(
      id: json['_id'] as String,
        nombre: json['nombre'] as String,
      detalle: json['detalle'] as String,
      imgPortada: json['imgPortada'] as String,
      direccion: json['direccion'] as String,
      whatsapp: json['whatsapp'] as String,
      celular: json['celular'] as String,
      paginaWeb: json['paginaWeb'] as String,
      paginaFacebook: json['paginaFacebook'] as String,
      correoElectronico: json['correoElectronico'] as String,
      instagram: json['instagram'] as String,
      mapa: json['mapa'] as String,
      visitas: json['visitas'] as String,
      latitud: json['latitud'] as String,
      longitud: json['longitud'] as String,
      comentarios: json['comentarios'] as String,
      puntuacion: json['puntuacion'] as String,
      delivery: json['delivery'] as String,
      horario: json['horario'] as String,
      propietario: json['propietario'] as String,
      oferta: json['oferta'] as String,
      promocion: json['promocion'] as String,
      descuento: json['descuento'] as String,
      formasPago: json['formasPago'] as String

    );
  }

  @override
  String toString() {
    return 'Actividad{id: $id, detalle: $detalle, imgPortada: $imgPortada, direccion: $direccion, whatsapp: $whatsapp, celular: $celular, paginaWeb: $paginaWeb, paginaFacebook: $paginaFacebook, correoElectronico: $correoElectronico, instagram: $instagram, mapa: $mapa, visitas: $visitas, latitud: $latitud, longitud: $longitud, comentarios: $comentarios, puntuacion: $puntuacion, delivery: $delivery, horario: $horario, propietario: $propietario, oferta: $oferta, promocion: $promocion, descuento: $descuento, formasPago: $formasPago}';
  }


}
