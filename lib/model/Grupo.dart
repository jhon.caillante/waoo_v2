import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Grupo {
  final String id;
  final String nombre;
  final String descripcion;
  final String img;
  final String grupo;


  Grupo({
    @required this.id,
    @required this.nombre,
    @required this.descripcion,
    @required this.img,
    this.grupo,
  });

  factory Grupo.fromJson(Map<dynamic, dynamic> json) {
    return Grupo(
        id: json['_id'] as String,
        nombre: json['nombre'] as String,
        descripcion: json['descripcion'] as String,
        img: json['img'] as String,
        grupo: json['grupo'] as String,

    );
  }

  @override
  String toString() {
    return 'Grupo{id: $id, nombre: $nombre, descripcion: $descripcion, img: $img, grupo: $grupo}';
  }


}
