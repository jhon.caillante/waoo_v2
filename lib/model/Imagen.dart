import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Imagen {
  final String id;
  final String actividad;
  final String img;


  Imagen({
    @required this.id,
    @required this.actividad,
    @required this.img,
  });

  factory Imagen.fromJson(Map<dynamic, dynamic> json) {
    return Imagen(
      id: json['_id'] as String,
      actividad: json['actividad'] as String,
      img: json['img'] as String,

    );
  }




}
