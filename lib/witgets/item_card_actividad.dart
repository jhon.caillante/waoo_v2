import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:waoo/model/Actividad.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

typedef void TapCallback(Actividad actividad);

class ItemCardActividad extends StatelessWidget {
  Actividad actividad;
  final TapCallback onTap;
  ItemCardActividad({this.actividad, this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130,
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: Material(
          borderRadius: BorderRadius.circular(9.0),
          elevation: 5.0,
          child: InkWell(
            onTap: () {
              //onTap(index);
              onTap(actividad);
            },
            borderRadius: BorderRadius.circular(9.0),
            child: Padding(
              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
              child: Row(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          child: Material(
                            elevation: 4.0,
                            borderRadius: BorderRadius.only(
                              // bottomLeft: Radius.circular(9.0),
                              topLeft: Radius.circular(9.0),
                            ),
                            // shape: CircleBorder(),
                            clipBehavior: Clip.hardEdge,
                            color: Colors.transparent,
                            child: CachedNetworkImage(
                              fit: BoxFit.fill,
                              imageUrl:
                                  "http://www.backend.embolvente.com/upload/${actividad.imgPortada}",
                              placeholder: (context, url) => SpinKitPulse(
                                color: Colors.deepPurple,
                                size: 100.0,
                              ),
                              errorWidget: (context, url, error) => Image(
                                  image: AssetImage(
                                      'assets/images/defaultImage.png')),
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 1.0,
                          decoration: new BoxDecoration(
                            borderRadius: new BorderRadius.only(
                              bottomLeft: Radius.circular(9.0),
                            ),
                            color: Colors.purple,
                          ),
                          padding: EdgeInsets.all(6.0),
                          child: Text(
                            "Abierto",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "${actividad.nombre}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline,
                                    color: Colors.purple),
                              ),

                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.star,
                                color: Colors.purple,
                                size: 20.0,
                                semanticLabel: 'Exelente Servicio',
                              ),
                              Text("${actividad.puntuacion} Valoración"),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.mode_comment,
                                color: Colors.purple,
                                size: 20.0,
                                semanticLabel: 'Opiniones',
                              ),
                              Text("${actividad.comentarios} Comentarios"),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.remove_red_eye,
                                color: Colors.purple,
                                size: 20.0,
                                semanticLabel: '201 Visualizaciones',
                              ),
                              Text("${actividad.visitas} Visualizaciones"),
                            ],
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: (actividad.nombre=="NO")?Container():  Container(
                                  padding: EdgeInsets.all(4.0),
                                  decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.circular(8.0),
                                    color: Colors.grey,
                                  ),
                                  // width: 80,
                                  //height: 35,
                                  child: Text(
                                    "Ofertas",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
