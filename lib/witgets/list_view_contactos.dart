import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:waoo/model/Actividad.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:waoo/model/Contacto.dart';

typedef void TapCallback(Contacto contacto);

class ListViewContactos extends StatelessWidget {
  List<Contacto> contactos;
  final TapCallback onTap;
  ListViewContactos({this.contactos, this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.all(0),
            itemCount: (contactos.length!=null)?contactos.length:0,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 50,

                child: Material(
                  borderRadius: BorderRadius.circular(0.0),
                  elevation: 0.0,
                  child: InkWell(
                    onTap: () {
                      //onTap(index);
                      onTap(contactos[index]);
                    },
                    borderRadius: BorderRadius.circular(0.0),
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[

                          Container(
                            child: Row(
                             // crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                contactos[index].icono,
                                Text("${contactos[index].nombre}", style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),),

                              ],
                            ),
                          ),


                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.purple,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }
        ),
      ),
    );
  }
}
