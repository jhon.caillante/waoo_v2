import 'package:flutter/material.dart';
import 'package:waoo/model/Grupo.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
typedef void TapCallback(Grupo grupo);

class ItemCardGrupo extends StatelessWidget {
  Grupo grupo;
  final TapCallback onTap;
  ItemCardGrupo({this.grupo,this.onTap});
  @override
  Widget build(BuildContext context) {


    return Padding(
      padding: EdgeInsets.all(5.0),
      child: Material(
        borderRadius: BorderRadius.circular(9.0),
        elevation: 5.0,
        child: InkWell(
          borderRadius: BorderRadius.circular(9.0),
          child: Padding(
            padding: EdgeInsets.all(0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Container(
                  // height: MediaQuery.of(context).size.height * 0.3,
                  width: MediaQuery.of(context).size.width * 1.0,
                  child: Material(
                    //elevation: 1.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(9.0),
                          topRight: const Radius.circular(9.0),
                        )
                      // side: BorderSide(color: Colors.red)
                    ),
                    //  shape: CircleBorder(),
                    clipBehavior: Clip.hardEdge,
                    color: Colors.transparent,
                    child: CachedNetworkImage(
                      imageUrl: "http://www.backend.embolvente.com/upload/${grupo.img}",
                      placeholder: (context, url) => SpinKitPulse(
                        color: Colors.deepPurple,
                        size: 120.0,
                      ),
                      errorWidget: (context, url, error) =>Image(image: AssetImage('assets/images/defaultImage.png')),
                    ),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.fromLTRB(10.0, 10.0, 6.0, 0.0),
                  child: Text(
                    "${grupo.nombre}",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10.0, 0.0, 6.0, 10.0),
                  child: Text(
                    "${grupo.descripcion}",
                    style: TextStyle(fontSize: 9.0),
                  ),
                ),
              ],
            ),
          ),
          onTap: () {
            onTap(grupo);

            /* Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DenunciaPage()),
                  );*/
          },
        ),
      ),
    );
  }
}
