import 'package:flutter/material.dart';

typedef void TapCallback(int index);

class GridViewItem extends StatelessWidget {
  final TapCallback onTap;
  GridViewItem({this.onTap});
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 155) / 2;
    final double itemWidth = size.width / 2;

    // TODO: implement build
    return GridView.count(
      childAspectRatio: (itemWidth / itemHeight),
      crossAxisCount: 3,
      children: List.generate(100, (index) {
        return Padding(
          padding: EdgeInsets.all(5.0),
          child: Material(
            borderRadius: BorderRadius.circular(9.0),
            elevation: 5.0,
            child: InkWell(
              borderRadius: BorderRadius.circular(9.0),
              child: Padding(
                padding: EdgeInsets.all(0.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(10.0, 10.0, 6.0, 10.0),
                      child: Text(
                        "Titulo $index",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      // height: MediaQuery.of(context).size.height * 0.3,
                      width: MediaQuery.of(context).size.width * 1.0,
                      child: Material(
                        elevation: 4.0,
                        // shape: CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: AssetImage('assets/images/defaultImage.png'),
                          fit: BoxFit.cover,
                          width: 70.0,
                          height: 70.0,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(10.0, 8.0, 6.0, 10.0),
                      child: Text(
                        "Lorem Ipsum  ",
                        style: TextStyle(fontSize: 9.0),
                      ),
                    ),
                  ],
                ),
              ),
              onTap: () {
                onTap(index);
                print("asf");
                /* Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DenunciaPage()),
                  );*/
              },
            ),
          ),
        );
      }),
    );
  }
}
