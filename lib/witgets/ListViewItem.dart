import 'package:flutter/material.dart';
//import 'package:waoo/pages/Detalle.dart';
typedef void TapCallback(int index);
class ListViewItem extends StatelessWidget {

  final TapCallback onTap;
  ListViewItem({this.onTap});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: 100,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          height: 130,
          child: Padding(
            padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: Material(
              borderRadius: BorderRadius.circular(9.0),
              elevation: 5.0,
              child: InkWell(
                onTap: () {
                  onTap(index);

                },
                borderRadius: BorderRadius.circular(9.0),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: Material(
                          elevation: 4.0,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(9.0),
                              topLeft: Radius.circular(9.0)),
                          // shape: CircleBorder(),
                          clipBehavior: Clip.hardEdge,
                          color: Colors.transparent,
                          child: Ink.image(
                            image: AssetImage(
                                'assets/images/defaultImage.png'),
                            fit: BoxFit.cover,
                            width: 70.0,
                            //height: 70.0,
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                //crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "NOMBRE $index",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        decoration:
                                        TextDecoration.underline,
                                        color: Colors.purple),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(7.0),
                                    decoration: new BoxDecoration(
                                      borderRadius:
                                      new BorderRadius.circular(
                                          8.0),
                                      color: Colors.purple,
                                    ),
                                    // width: 80,
                                    //height: 35,
                                    child: Text(
                                      "Ofertas",
                                      style: TextStyle(
                                          color: Colors.white),
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.star,
                                    color: Colors.purple,
                                    size: 20.0,
                                    semanticLabel: 'Exelente Servicio',
                                  ),
                                  Text("4.5 Exelente Servicio"),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.location_on,
                                    color: Colors.purple,
                                    size: 20.0,
                                    semanticLabel: 'Zona San Pedro',
                                  ),
                                  Text("Zona San Pedro"),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.mode_comment,
                                    color: Colors.purple,
                                    size: 20.0,
                                    semanticLabel: 'Opiniones',
                                  ),
                                  Text("20 Opiniones"),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.remove_red_eye,
                                    color: Colors.purple,
                                    size: 20.0,
                                    semanticLabel:
                                    '201 Visualizaciones',
                                  ),
                                  Text("201 Visualizaciones"),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) =>
      const Divider(),
    );
  }
}
