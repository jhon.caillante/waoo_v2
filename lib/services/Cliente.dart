import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:waoo/model/Actividad.dart';
import 'package:waoo/model/Grupo.dart';
import 'package:waoo/model/Imagen.dart';

class Cliente {
  final apiUrl = "http://www.backend.embolvente.com";
 // final apiUrl = "http://159.65.179.209";


  Future<List<Grupo>> getDataGrupos() async {
    var url_ = "${apiUrl}/grupo";
    final response = await http.get(url_);
    if (response.statusCode == 200) {
      print("-----------------");
      print(response.body);
      print("-----------------");

      List<dynamic> body = convert.jsonDecode(response.body);
      List<Grupo> result = body
          .map(
            (dynamic item) => Grupo.fromJson(item),
          )
          .toList();
      return result;
    } else {
      print("Error : ${response.body}");
      return null;
    }
  }

  Future<List<Grupo>> getDataSubGrupos(Grupo grupo) async {
    var url_ = "${apiUrl}/subgrupo/${grupo.id}";
    final response = await http.get(url_);
    if (response.statusCode == 200) {
      print("-----------------");
      print(response.body);
      print("-----------------");

      List<dynamic> body = convert.jsonDecode(response.body);
      List<Grupo> result = body
          .map(
            (dynamic item) => Grupo.fromJson(item),
      )
          .toList();
      return result;
    } else {
      print("Error : ${response.body}");
      return null;
    }
  }

  Future<List<Actividad>> getDataActividades(Grupo grupo) async {
    var url_ = "${apiUrl}/actividades/${grupo.id}";
    final response = await http.get(url_);
    if (response.statusCode == 200) {
      print("-----------------");
      print(response.body);
      print("-----------------");

      List<dynamic> body = convert.jsonDecode(response.body);
      List<Actividad> result = body
          .map(
            (dynamic item) => Actividad.fromJson(item),
      )
          .toList();
      return result;
    } else {
      print("Error : ${response.body}");
      return null;
    }
  }

  Future<List<Imagen>> getDataImagenes(Actividad actividad) async {
    var url_ = "${apiUrl}/imagenes/${actividad.id}";
    final response = await http.get(url_);
    if (response.statusCode == 200) {
      print("-----------------");
      print(response.body);
      print("-----------------");

      List<dynamic> body = convert.jsonDecode(response.body);
      List<Imagen> result = body
          .map(
            (dynamic item) => Imagen.fromJson(item),
      )
          .toList();
      return result;
    } else {
      print("Error : ${response.body}");
      return null;
    }
  }


}
