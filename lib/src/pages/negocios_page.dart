import 'package:flutter/material.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:waoo/src/pages/negocios_detalle.dart';
import 'package:waoo/src/pages/nivel_page.dart';
import 'package:waoo/model/Actividad.dart';
import 'package:waoo/model/Grupo.dart';

import 'package:waoo/services/Cliente.dart';

import 'package:waoo/witgets/item_card_actividad.dart';
import 'package:waoo/witgets/item_card_grupo.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';

class NegociosPage extends StatelessWidget {
  static final String routeName = 'negocios';
  Grupo grupo;
  NegociosPage({this.grupo});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Waoo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: NegociosPage_(
        grupo: grupo,
      ),
    );
  }
}

class NegociosPage_ extends StatefulWidget {
  final Color _tabBackgroundColor = const Color(0xFF6A0B8F);
  Grupo grupo;
  NegociosPage_({this.grupo});

  _MainState createState() => _MainState();
}

class _MainState extends State<NegociosPage_> {

  final TextEditingController _searchQuery = new TextEditingController();
  List<BubbleBottomBarItem> bubbleBottomBarItemList = [];
 // List<Grupo> grupos;
  List<Actividad> actividades;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDataActividades(widget.grupo);
  }



  selectItemActividad(Actividad actividad) {
    print("actividad ${actividad.toString()}");

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Detalle(
       actividad: actividad,
      )),
    );
  }

  Future<void> getDataActividades(Grupo grupo) async {
    var actividades_ = await Cliente().getDataActividades(grupo);
    setState(() {
      //  topTitle = "Productos y Servicios";
      actividades = actividades_;
    });
  }


  Widget appBarTitle = Text("Bienvenido");
  Icon actionIcon = Icon(
    Icons.search,
    color: Colors.white,
  );

  @override
  void dispose() {

    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    final double itemHeight = (size.height - kToolbarHeight - 120) / 2;
    final double itemWidth = size.width / 2;

    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      appBar: AppBar(
        title:Text('${widget.grupo.nombre}'),
        backgroundColor: Color.fromRGBO(106, 11, 143, 10),
        actions:<Widget>[
          IconButton(
            icon: Icon( Icons.search),
            onPressed: (){},
          ),
          SizedBox(height: 8000),

        ],

      ),
      body: ListView.separated(
        padding: const EdgeInsets.all(8),
        itemCount:
        (actividades != null) ? actividades.length : 0,
        itemBuilder: (BuildContext context, int index) {
          return ItemCardActividad(
            actividad: actividades[index],
            onTap: selectItemActividad,
          );
        },
        separatorBuilder: (BuildContext context, int index) =>
        const Divider(),
      ),

    );
  }


}
