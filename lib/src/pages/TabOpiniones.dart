import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class TabOpiniones extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(5.0),
          child: Card(
            elevation: 8.0,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Calificar este negocio",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),
                  ),
                  Text(
                    "Comparte tu opinión",
                    style: TextStyle(fontSize: 12.0),
                  ),
                  Divider(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.star_border),
                        tooltip: 'Puntuacion 1',
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(Icons.star_border),
                        tooltip: 'Puntuacion 2',
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(Icons.star_border),
                        tooltip: 'Puntuacion 3',
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(Icons.star_border),
                        tooltip: 'Puntuacion 4',
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(Icons.star_border),
                        tooltip: 'Puntuacion 5',
                        onPressed: () {},
                      ),
                    ],
                  ),
                  Divider(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Calificar y reseñas",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15.0),
                      ),
                      IconButton(
                        icon: Icon(Icons.arrow_forward),
                        tooltip: 'Puntuacion 5',
                        onPressed: () {},
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * 0.40,
                        child: Column(
                          children: <Widget>[
                            Text(
                              "4.2",
                              style: TextStyle(fontSize: 35.0),
                            ),
                            Container(
                              width: 300,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.star,
                                    color: Colors.purple,
                                    size: 15.0,
                                    semanticLabel: 'Puntuacion 1',
                                  ),
                                  Icon(
                                    Icons.star_half,
                                    color: Colors.purple,
                                    size: 15.0,
                                    semanticLabel: 'Puntuacion 2',
                                  ),
                                  Icon(
                                    Icons.star_border,
                                    color: Colors.purple,
                                    size: 15.0,
                                    semanticLabel: 'Puntuacion 3',
                                  ),
                                  Icon(
                                    Icons.star_border,
                                    color: Colors.purple,
                                    size: 15.0,
                                    semanticLabel: 'Puntuacion 4',
                                  ),
                                  Icon(
                                    Icons.star_border,
                                    color: Colors.purple,
                                    size: 15.0,
                                    semanticLabel: 'Puntuacion 5',
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.45,
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text("5"),
                                Padding(
                                  padding: EdgeInsets.all(0.0),
                                  child: new LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    animation: true,
                                    lineHeight: 10.0,
                                    animationDuration: 2000,
                                    percent: 0.9,
                                    // center: Text("90.0%"),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Colors.purple,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text("4"),
                                Padding(
                                  padding: EdgeInsets.all(0.0),
                                  child: new LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    animation: true,
                                    lineHeight: 10.0,
                                    animationDuration: 1090,
                                    percent: 0.3,
                                    // center: Text("90.0%"),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Colors.purple,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text("3"),
                                Padding(
                                  padding: EdgeInsets.all(0.0),
                                  child: new LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    animation: true,
                                    lineHeight: 10.0,
                                    animationDuration: 1080,
                                    percent: 0.4,
                                    // center: Text("90.0%"),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Colors.purple,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text("2"),
                                Padding(
                                  padding: EdgeInsets.all(0.0),
                                  child: new LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    animation: true,
                                    lineHeight: 10.0,
                                    animationDuration: 1070,
                                    percent: 0.5,
                                    // center: Text("90.0%"),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Colors.purple,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text("1"),
                                Padding(
                                  padding: EdgeInsets.all(0.0),
                                  child: new LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width *
                                        0.35,
                                    animation: true,
                                    lineHeight: 10.0,
                                    animationDuration: 1060,
                                    percent: 0.6,
                                    // center: Text("90.0%"),
                                    linearStrokeCap: LinearStrokeCap.roundAll,
                                    progressColor: Colors.purple,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  Divider(
                    height: 20.0,
                    color: Colors.purple,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Principal reseñas positivas",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15.0),
                      ),
                      IconButton(
                        icon: Icon(Icons.arrow_forward),
                        tooltip: 'Ver mas',
                        onPressed: () {},
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.person_pin,
                              color: Colors.purple,
                              size: 25.0,
                              semanticLabel: 'Pablo',
                            ),
                            Text(
                              "Jhon Caillante",
                              style: TextStyle(fontSize: 15.0),
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.purple,
                              size: 15.0,
                              semanticLabel: 'Puntuacion 1',
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.purple,
                              size: 15.0,
                              semanticLabel: 'Puntuacion 2',
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.purple,
                              size: 15.0,
                              semanticLabel: 'Puntuacion 3',
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.purple,
                              size: 15.0,
                              semanticLabel: 'Puntuacion 4',
                            ),
                            Icon(
                              Icons.star_half,
                              color: Colors.purple,
                              size: 15.0,
                              semanticLabel: 'Puntuacion 5',
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.more_vert),
                        tooltip: 'Ver mas',
                        onPressed: () {},
                      ),
                    ],
                  ),
                  Text(
                      "Me causó exactamente la impresión contraria. Uno de los lugares con mejor comida de mar y en comparación muy buenos precios. Comimos 3 platos por 90 bs."),
                      
                ],
              ),
            ),
            
          ),
        ),
        _crearBoton(context),
      ],
    ));
  }

  
  Widget _crearBoton(BuildContext context){
   return Padding(
     padding: EdgeInsets.all(2),
     child: Container(
       width: double.infinity,
       height: 100,
       decoration: BoxDecoration(
         color:Colors.grey.withOpacity(0.15),
         borderRadius: BorderRadius.circular(100)
       ),
       child: Row(children: <Widget>[
         Container(
           margin:EdgeInsets.symmetric(horizontal:15.0),
           alignment: Alignment.centerLeft,
           width: 120,
           height: 50,
           decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(100),
             color: Colors.purple,
           ),
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.center,
             children: <Widget>[
               SizedBox(width: 20,),
               Text('LLAMAR', style: TextStyle(color: Colors.white),),
               Icon(Icons.call,size: 35.0,color: Colors.white,),
             ],
           ) 
         ),

         Container(
           margin:EdgeInsets.symmetric(horizontal:15.0),
           alignment: Alignment.centerLeft,
           width: 150,
           height: 50,
           decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(100),
             color: Colors.white,
           ),
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.center,
             children: <Widget>[
               SizedBox(width: 20,),
               Text('WHATSAPP', style: TextStyle(color: Colors.black),),
               Icon(Icons.call,size: 35.0,color: Colors.green,),
             ],
           ) 
         ),
       ],),
    
     ),
   );
 }
}