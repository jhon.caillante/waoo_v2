import 'package:flutter/material.dart';
import 'package:waoo/src/pages/home_page.dart';


class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          _pagina1(),
          //_pagina2(),
          HomePage(),
        ],
      ),
    );
  }

  Widget _pagina1(){
    return Stack(
      children: <Widget>[
        _imagenFondo(),
        _textos(),
      ],
    );
  }
  
  Widget _imagenFondo(){
    return Container(
      width : double.infinity,
      height: double.infinity,
      child: Image(
        image: AssetImage('assets/waoo.gif'),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _textos(){
    return SafeArea(
      child: Column(
        children: <Widget>[
          Expanded(child: Container()),
          Icon(Icons.keyboard_arrow_down, size:50.0, color:Colors.white)
        ],
      ),
    );
  }

  
}
