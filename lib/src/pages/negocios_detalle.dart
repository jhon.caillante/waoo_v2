import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:waoo/model/Actividad.dart';
import 'package:waoo/model/Contacto.dart';
import 'package:waoo/model/Imagen.dart';
import 'package:waoo/services/Cliente.dart';
import 'package:waoo/src/pages/TabDireccion.dart';
import 'package:waoo/src/pages/TabImagenes.dart';
import 'package:waoo/src/pages/TabOpiniones.dart';
import 'package:waoo/src/pages/TabServicios.dart';
import 'package:waoo/tools/launch_contact.dart';

class Detalle extends StatelessWidget {
  static final String routeName = 'detalle';
  Actividad actividad;
  Detalle({this.actividad});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WAOO',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: DetallePage(
        actividad: actividad,
      ),
    );
  }
}

class DetallePage extends StatefulWidget {
  Actividad actividad;
  DetallePage({this.actividad});
  final Color _tabBackgroundColor = const Color(0xFF6A0B8F);
  _DetalleState createState() => _DetalleState();
}

class _DetalleState extends State<DetallePage> {
  var currentIndex = 0;
  List<BubbleBottomBarItem> bubbleBottomBarItemList = [];
  var tabIndex = 0;
  List<Widget> listTabs;
  List<Imagen> imagenes;
  @override
  void initState() {
    super.initState();

    getImagenes(widget.actividad);
    
  }

  getImagenes(Actividad actividad) async{
    var imagenes_ = await Cliente().getDataImagenes(actividad);

    setState(() {
      imagenes = imagenes_;

    });
    dataMenu();

  }

  selectItemContact(Contacto contacto) async {
    print("contacto paul::: ${contacto.toString()}");
    LaunchContacto().selectItemContact(contacto);
  }
  changePage(int i) {
    print("changePage $i");
    setState(() {
      currentIndex = i;
      tabIndex = i;
    });
  }

  dataMenu() {
    setState(() {
      listTabs = [
        TabServicios(
          actividad: widget.actividad,
          onTap: selectItemContact,
        ),
      //  TabDireccion(),
        TabImagenes(
          imagenes: imagenes,
        ),
        TabOpiniones()
      ];
    });

    bubbleBottomBarItemList.add(new BubbleBottomBarItem(
        backgroundColor: Colors.deepPurple,
        icon: Icon(
          Icons.assignment_ind,
          color: widget._tabBackgroundColor,
        ),
        activeIcon: Icon(
          Icons.assignment_ind,
          color: Colors.deepPurple,
        ),
        title: Text("Servicios")));


    bubbleBottomBarItemList.add(new BubbleBottomBarItem(
        backgroundColor: Colors.deepPurple,
        icon: Icon(
          Icons.image,
          color: widget._tabBackgroundColor,
        ),
        activeIcon: Icon(
          Icons.image,
          color: Colors.deepPurple,
        ),
        title: Text("Fotos")));

    bubbleBottomBarItemList.add(new BubbleBottomBarItem(
        backgroundColor: Colors.deepPurple,
        icon: Icon(
          Icons.forum,
          color: widget._tabBackgroundColor,
        ),
        activeIcon: Icon(
          Icons.forum,
          color: Colors.deepPurple,
        ),
        title: Text("Opiniones")));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      appBar: AppBar(
        title: Text(
          "${widget.actividad.nombre}",
          style: TextStyle(fontSize: 13.0),
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => {
            //Navigator.push(context,MaterialPageRoute(builder: (context) => Main()),)
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.notifications),
            tooltip: 'Notificaciones',
            onPressed: () {},
          ),
          IconButton(
            icon: const Icon(Icons.favorite),
            tooltip: 'Favoritos',
            onPressed: () {},
          ),
          IconButton(
            icon: const Icon(Icons.share),
            tooltip: 'Compartir',
            onPressed: () {},
          ),
        ],
      ),
      body: Container(
        child: (listTabs!=null)? listTabs[tabIndex]:Center(
          child: Text("Cargando.."),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
        backgroundColor: Colors.purple,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      bottomNavigationBar: (bubbleBottomBarItemList.length!=0)? BubbleBottomBar(
        opacity: .2,
        currentIndex: currentIndex,
        onTap: changePage,
        borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
        elevation: 8,
        fabLocation: BubbleBottomBarFabLocation.end, //new
        hasNotch: true, //new
        hasInk: true, //new, gives a cute ink effect
        inkColor: Colors.black12, //optional, uses theme color if not specified
        items: bubbleBottomBarItemList,
      ):null,
    );
  }
}
