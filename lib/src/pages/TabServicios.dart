

import 'package:flutter/material.dart';
import 'package:waoo/model/Actividad.dart';
import 'package:waoo/model/Contacto.dart';
//import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:waoo/witgets/list_view_contactos.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
typedef void TapCallback(Contacto contacto);
class TabServicios extends StatelessWidget {
  Actividad actividad;
  final TapCallback onTap;
TabServicios({this.actividad, this.onTap});

  List<Contacto> getListContactos(Actividad actividad) {
    List<Contacto> resp = [];

    if (actividad.whatsapp != null) {
      resp.add(Contacto(
          tipo: "ws",
          icono: Icon(
            MdiIcons.whatsapp,
            color: Colors.purple,
          ),
          nombre: "${actividad.whatsapp}",
          recurso: "${actividad.whatsapp}"));
    }

    if (actividad.celular != null) {
      resp.add(Contacto(
          tipo: "cell",
          icono: Icon(
            MdiIcons.cellphone,
            color: Colors.purple,
          ),
          nombre: "${actividad.celular}",
          recurso: "${actividad.celular}"));
    }

    if (actividad.paginaWeb != null) {
      resp.add(Contacto(
          tipo: "web",
          icono: Icon(
            MdiIcons.web,
            color: Colors.purple,
          ),
          nombre: "${actividad.paginaWeb}",
          recurso: "${actividad.paginaWeb}"));
    }

    if (actividad.correoElectronico != null) {
      resp.add(Contacto(
          tipo: "correo",
          icono: Icon(
            MdiIcons.email,
            color: Colors.purple,
          ),
          nombre: "${actividad.correoElectronico}",
          recurso: "${actividad.correoElectronico}"));
    }

    if (actividad.paginaFacebook != null) {
      resp.add(Contacto(
          tipo: "facebook",
          icono: Icon(
            MdiIcons.facebook,
            color: Colors.purple,
          ),
          nombre: "${actividad.paginaFacebook}",
          recurso: "${actividad.paginaFacebook}"));
    }

    if (actividad.instagram != null) {
      resp.add(Contacto(
          tipo: "instagram",
          icono: Icon(
            MdiIcons.instagram,
            color: Colors.purple,
          ),
          nombre: "${actividad.instagram}",
          recurso: "${actividad.instagram}"));
    }

    return resp;
  }

  @override
  Widget build(BuildContext context) {
    final _titulo = TextStyle(fontWeight: FontWeight.bold);
    return ListView(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width * 1.0,
          // height: 200,
          child: Padding(
            padding: EdgeInsets.all(0.0),
            child: Card(
              elevation: 8.0,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Productos/Servicios",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20.0),
                    ),
                    Divider(
                      height: 20.0,
                    ),
                    Text("${actividad.detalle}"),
                  ],
                ),
              ),
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 1.0,
          //  height: 130,
          child: Padding(
            padding: EdgeInsets.all(0.0),
            child: Card(
              elevation: 8.0,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Horario de atención",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20.0),
                    ),
                    Divider(
                      height: 20.0,
                    ),
                    Text("${actividad.horario}"),
                  ],
                ),
              ),
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 1.0,
          // height: 180,
          child: Padding(
            padding: EdgeInsets.all(0.0),
            child: Card(
              elevation: 8.0,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Formas de pago",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20.0),
                    ),
                    Divider(
                      height: 20.0,
                    ),
                    Text("${actividad.formasPago}"),
                  ],
                ),
              ),
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 1.0,
          //  height: 250,
          child: Padding(
            padding: EdgeInsets.all(0.0),
            child: Card(
              elevation: 8.0,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Información de contacto",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20.0),
                    ),
                    Divider(
                      height: 20.0,
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.person_outline,
                          color: Colors.purple,
                        ),
                        Text("Propietario : ${actividad.propietario} "),
                      ],
                    ),
                    Divider(
                      height: 20.0,
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.purple,
                        ),
                        Text("${actividad.direccion}"),
                      ],
                    ),
                    Divider(
                      height: 20.0,
                    ),
                    ListViewContactos(
                      contactos: getListContactos(actividad),
                      onTap: onTap,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 1.0,
         // height: 400,
          child: Padding(
            padding: EdgeInsets.all(0.0),
            child: Card(
              elevation: 8.0,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Mapa de ubicaciónnnn",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20.0),
                    ),

Text(""),

                    Container(
                      height: 200,
                      width: double.infinity,
                      child: CachedNetworkImage(
                        //imageUrl: "http://www.backend.embolvente.com/upload/${actividad.mapa}",
                        imageUrl: "http://www.backend.embolvente.com/upload/galeria/ironman.jpg",
                        placeholder: (context, url) => SpinKitPulse(
                          color: Colors.deepPurple,
                          size: 120.0,
                        ),
                        errorWidget: (context, url, error) =>Image(image: AssetImage('assets/mapa.png')),
                      ),
                    ),
                    Text(""),
                  ],
                ),
              ),
            ),
          ),
        ),


        
        _crearBoton(context),
        
        

        // Container(
        //   width: MediaQuery.of(context).size.width * 1.0,
        //   height: 400,
        //   child: Padding(padding: EdgeInsets.all(0.0),

        //     child: Card(
        //       elevation: 8.0,
        //       child: Padding(padding: EdgeInsets.all(10.0),
        //         child: Column(
        //           crossAxisAlignment: CrossAxisAlignment.start,
        //           children: <Widget>[
        //             Text("Mapa de ubicación", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
        //            Container(
        //              height: 300,
        //              width: 200,
        //              child: GoogleMap(
        //                mapType: MapType.hybrid,
        //               initialCameraPosition: _kGooglePlex,
        //             //   onMapCreated: (GoogleMapController controller) {
        //                //  _controller.complete(controller);
        //              //  },
        //              ),
        //            )


        //           ],
        //         ),
        //       ),
        //     ),
        //   ),

        // ),
        
      ],
    );
  }

  Widget _crearBoton(BuildContext context){
   return Padding(
     padding: EdgeInsets.all(2),
     child: Container(
       width: double.infinity,
       height: 100,
       decoration: BoxDecoration(
         color:Colors.grey.withOpacity(0.15),
         borderRadius: BorderRadius.circular(100)
       ),
       child: Row(children: <Widget>[
         Container(
           margin:EdgeInsets.symmetric(horizontal:15.0),
           alignment: Alignment.centerLeft,
           width: 120,
           height: 50,
           decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(100),
             color: Colors.purple,
           ),
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.center,
             children: <Widget>[
               SizedBox(width: 20,),
               Text('LLAMAR', style: TextStyle(color: Colors.white),),
               Icon(Icons.call,size: 35.0,color: Colors.white,),
             ],
           ) 
         ),

         Container(
           margin:EdgeInsets.symmetric(horizontal:15.0),
           alignment: Alignment.centerLeft,
           width: 150,
           height: 50,
           decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(100),
             color: Colors.white,
           ),
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.center,
             children: <Widget>[
               SizedBox(width: 20,),
               Text('WHATSAPP', style: TextStyle(color: Colors.black),),
               Icon(Icons.call,size: 35.0,color: Colors.green,),
             ],
           ) 
         ),
       ],),
    
     ),
   );
 }
}