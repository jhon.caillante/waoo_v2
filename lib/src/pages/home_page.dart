import 'package:flutter/material.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:waoo/src/pages/nivel_page.dart';
import 'package:waoo/model/Actividad.dart';
import 'package:waoo/model/Grupo.dart';

import 'package:waoo/services/Cliente.dart';

import 'package:waoo/witgets/item_card_actividad.dart';
import 'package:waoo/witgets/item_card_grupo.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';

class HomePage extends StatelessWidget {
  static final String routeName = 'menu';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Waoo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: MainPagehome(),
    );
  }
}

class MainPagehome extends StatefulWidget {
  final Color _tabBackgroundColor = const Color(0xFF6A0B8F);

  _MainState createState() => _MainState();
}

class _MainState extends State<MainPagehome> {

  final TextEditingController _searchQuery = new TextEditingController();
  List<BubbleBottomBarItem> bubbleBottomBarItemList = [];
  List<Grupo> grupos;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDataGrupos();
  }

  selectItemGrupo(Grupo grupo) async {

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NivelPage(
        grupo: grupo,
      )),
    );

  //  Navigator.pushNamed(context, NivelPage.routeName);
    print('grupo ${grupo.toString()} ');
  }



  Future<void> getDataGrupos() async {
    var grupos_ = await Cliente().getDataGrupos();
    setState(() {
    //  topTitle = "Productos y Servicios";
      grupos = grupos_;
    });
  }


  Widget appBarTitle = Text("Bienvenido");
  Icon actionIcon = Icon(
    Icons.search,
    color: Colors.white,
  );

  @override
  void dispose() {

    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    final double itemHeight = (size.height - kToolbarHeight - 120) / 2;
    final double itemWidth = size.width / 2;

    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      appBar: AppBar(
        title:Text('Bienvenido'),
        backgroundColor: Color.fromRGBO(106, 11, 143, 10),
        actions:<Widget>[
          IconButton(
            icon: Icon( Icons.search),
            onPressed: (){},
          ),
          SizedBox(height: 8000),

        ],

      ),
      body: GridView.count(
        childAspectRatio: (itemWidth / itemHeight),
        crossAxisCount: 2,
        children: List.generate(
            (grupos != null) ? grupos.length : 0, (index) {
          return ItemCardGrupo(
            grupo: grupos[index],
            onTap: selectItemGrupo,
          );
        }),
      ),

    );
  }


}
