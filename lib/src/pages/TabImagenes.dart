


import 'dart:math';

import 'package:flutter/material.dart';
import 'package:waoo/model/Imagen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
class TabImagenes extends StatelessWidget {
  List<Imagen> imagenes;
  TabImagenes({this.imagenes});
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      // Create a grid with 2 columns. If you change the scrollDirection to
      // horizontal, this produces 2 rows.
      crossAxisCount: 2,
      // Generate 100 widgets that display their index in the List.
      children: List.generate(imagenes.length, (index) {
        return Container(
          child: CachedNetworkImage(
            imageUrl: "http://www.backend.embolvente.com/upload/${imagenes[index].img}",
            placeholder: (context, url) => SpinKitPulse(
              color: Colors.deepPurple,
              size: 120.0,
            ),
            errorWidget: (context, url, error) =>Image(image: AssetImage('assets/images/defaultImage.png')),
          ),
        );
      }),
    );
  }

  Widget _crearBoton(BuildContext context){
   return Padding(
     padding: EdgeInsets.all(2),
     child: Container(
       width: double.infinity,
       height: 100,
       decoration: BoxDecoration(
         color:Colors.grey.withOpacity(0.15),
         borderRadius: BorderRadius.circular(100)
       ),
       child: Row(children: <Widget>[
         Container(
           margin:EdgeInsets.symmetric(horizontal:15.0),
           alignment: Alignment.centerLeft,
           width: 120,
           height: 50,
           decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(100),
             color: Colors.purple,
           ),
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.center,
             children: <Widget>[
               SizedBox(width: 20,),
               Text('LLAMAR', style: TextStyle(color: Colors.white),),
               Icon(Icons.call,size: 35.0,color: Colors.white,),
             ],
           ) 
         ),

         Container(
           margin:EdgeInsets.symmetric(horizontal:15.0),
           alignment: Alignment.centerLeft,
           width: 150,
           height: 50,
           decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(100),
             color: Colors.white,
           ),
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.center,
             children: <Widget>[
               SizedBox(width: 20,),
               Text('WHATSAPP', style: TextStyle(color: Colors.black),),
               Icon(Icons.call,size: 35.0,color: Colors.green,),
             ],
           ) 
         ),
       ],),
    
     ),
   );
 }

  Widget _fondoApp(){
    final gradiente = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: FractionalOffset(0.0,0.6),
          end: FractionalOffset(0.0,1.0),
          colors: [
            Color.fromRGBO(52, 54, 101, 1.0),
            Color.fromRGBO(35, 37, 57, 1.0)
          ]
        )
      ),
    );

    final cajaRosa = Transform.rotate(
      angle: -pi / 4.0,
      child:  Container(
        height: 360.0,
        width: 360.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(80.0),
          gradient: LinearGradient(
            colors:[
              Color.fromRGBO(236,98,188,1.0),
              Color.fromRGBO(241, 14, 172, 1.0)
            ] 
            )
        ),
      ),
    );
    
   
    return Stack(
      children: <Widget>[
        gradiente,
        Positioned(
          top: -100.0,
          child: cajaRosa,
        )
        
      ],
    );
  }

  Widget _botonesRedondeados(){
    return Table(
      children: [
        TableRow(
          children: [
            _crearBotonRedondeado( 'assets/cevi1.jpg'),
            _crearBotonRedondeado( 'assets/cevi2.jpg'),
            _crearBotonRedondeado( 'assets/cevi3.jpg'),
          ]
        ),
        TableRow(
          children: [
           _crearBotonRedondeado( 'assets/cevi4.jpg'),
           _crearBotonRedondeado( 'assets/cevi5.jpg'),
           _crearBotonRedondeado( 'assets/cevi6.jpg'),
          ]
        ),
      ],
    );
  }

Widget _crearBotonRedondeado(String imagen){
    return Container(
        height: 160.0,
        margin: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: Color.fromRGBO(106, 11, 143, 10),
          borderRadius: BorderRadius.circular(30.0),
          boxShadow:<BoxShadow>[
            BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0,5.0),
              spreadRadius: 3.0
            )
          ]
        ),
        
        child: 
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ClipRRect(
              borderRadius:  BorderRadius.circular(30.0),
              child:GestureDetector(
                onTap: (){
                  print('CLICK EN EL IMAGEN');
                  //NivelPage();
                  //Navigator.pushNamed(context, NegociosPage.routeName);
                  //Navigator.pushNamed(context, 'nivel',arguments: '');
                },
                child: Image(
                  image: AssetImage( imagen ),
                  height: 160.0,
                  fit:BoxFit.cover
                ),
              ),
            ),
            //Text(text, style: TextStyle(color: Colors.white,fontSize: 15.0),overflow:TextOverflow.ellipsis)

          ],
          
        ),
      
    );
  }

}
