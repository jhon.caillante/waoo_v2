import 'package:flutter/material.dart';

class TabDireccion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width * 1.0,
          height: 350,
          child: Padding(padding: EdgeInsets.all(0.0),
            child: Card(
              elevation: 8.0,
              child: Padding(padding: EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Ubicación de Mapa", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
                    Divider(
                      height: 20.0,
                    ),
                    Image(
                    image: AssetImage( 'assets/mapa.png' ),
                    fit:BoxFit.cover
                    ),
                  ],
                ),
              ),
            ),
          ),

        ),
        Container(
          width: MediaQuery.of(context).size.width * 1.0,
          height: 270,
          child: Padding(padding: EdgeInsets.all(0.0),
            child: Card(
              elevation: 8.0,
              child: Padding(padding: EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Información de contacto", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
                    Divider(
                      height: 20.0,
                    ),

                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.purple,
                        ),
                        Text("Edificio multicine Piso 7 Of 701 - La Paz"),
                      ],
                    ),

                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.call,
                          color: Colors.purple,
                        ),
                        Text("2430430 (Multicine)"),
                      ],
                    ),

                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.call,
                          color: Colors.purple,
                        ),
                        Text("2351687 (Prado)"),
                      ],
                    ),


                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.web_asset,
                          color: Colors.purple,
                        ),
                        Text("wwww.cevicheria.com)"),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.language,
                          color: Colors.purple,
                        ),
                        Text("facebook/cevicheriaJR)"),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.mail,
                          color: Colors.purple,
                        ),
                        Text("cevicheriajr@gmail.com)"),
                      ],
                    ),
                    Divider(
                      height: 20.0,
                    ),

                  ],
                ),
              ),
            ),
          ),

        ),
        _crearBoton(context),
        
      ],
    );
  }

  Widget _crearBoton(BuildContext context){
   return Padding(
     padding: EdgeInsets.all(2),
     child: Container(
       width: double.infinity,
       height: 100,
       decoration: BoxDecoration(
         color:Colors.grey.withOpacity(0.15),
         borderRadius: BorderRadius.circular(100)
       ),
       child: Row(children: <Widget>[
         Container(
           margin:EdgeInsets.symmetric(horizontal:15.0),
           alignment: Alignment.centerLeft,
           width: 120,
           height: 50,
           decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(100),
             color: Colors.purple,
           ),
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.center,
             children: <Widget>[
               SizedBox(width: 20,),
               Text('LLAMAR', style: TextStyle(color: Colors.white),),
               Icon(Icons.call,size: 35.0,color: Colors.white,),
             ],
           ) 
         ),

         Container(
           margin:EdgeInsets.symmetric(horizontal:15.0),
           alignment: Alignment.centerLeft,
           width: 150,
           height: 50,
           decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(100),
             color: Colors.white,
           ),
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.center,
             children: <Widget>[
               SizedBox(width: 20,),
               Text('WHATSAPP', style: TextStyle(color: Colors.black),),
               Icon(Icons.call,size: 35.0,color: Colors.green,),
             ],
           ) 
         ),
       ],),
    
     ),
   );
 }
}