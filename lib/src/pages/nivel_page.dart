import 'package:flutter/material.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:waoo/src/pages/negocios_page.dart';
import 'package:waoo/src/pages/nivel_page.dart';
import 'package:waoo/model/Actividad.dart';
import 'package:waoo/model/Grupo.dart';

import 'package:waoo/services/Cliente.dart';

import 'package:waoo/witgets/item_card_actividad.dart';
import 'package:waoo/witgets/item_card_grupo.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';

class NivelPage extends StatelessWidget {
  Grupo grupo;
  NivelPage({this.grupo});
  static final String routeName = 'nivel';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Waoo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: NivelPage_(
        grupo: grupo,
      ),
    );
  }
}

class NivelPage_ extends StatefulWidget {
  final Color _tabBackgroundColor = const Color(0xFF6A0B8F);
  Grupo grupo;
  NivelPage_({ this.grupo});
  _MainState createState() => _MainState();
}

class _MainState extends State<NivelPage_> {

  final TextEditingController _searchQuery = new TextEditingController();
  List<BubbleBottomBarItem> bubbleBottomBarItemList = [];
  List<Grupo> grupos;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDataGrupos(widget.grupo);
    print("grupo ${widget.grupo.nombre}");
  }

  selectItemGrupo(Grupo grupo) async {
  //  Navigator.pushNamed(context, NegociosPage.routeName);

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NegociosPage(
        grupo: grupo,
      )),
    );

    //  Navigator.pushNamed(context, NivelPage.routeName);
    print('grupo ${grupo.toString()} ');
  }



  Future<void> getDataGrupos(Grupo grupo) async {
    var grupos_ = await Cliente().getDataSubGrupos(grupo);
    setState(() {
      //  topTitle = "Productos y Servicios";
      grupos = grupos_;
    });
  }


  @override
  void dispose() {
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    final double itemHeight = (size.height - kToolbarHeight - 120) / 2;
    final double itemWidth = size.width / 2;

    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      appBar: AppBar(
        title:Text('${widget.grupo.nombre}'),
        backgroundColor: Color.fromRGBO(106, 11, 143, 10),
        actions:<Widget>[
          IconButton(
            icon: Icon( Icons.search),
            onPressed: (){},
          ),
          SizedBox(height: 8000),

        ],

      ),
      body: GridView.count(
        childAspectRatio: (itemWidth / itemHeight),
        crossAxisCount: 2,
        children: List.generate(
            (grupos != null) ? grupos.length : 0, (index) {
          return ItemCardGrupo(
            grupo: grupos[index],
            onTap: selectItemGrupo,
          );
        }),
      ),

    );
  }


}
